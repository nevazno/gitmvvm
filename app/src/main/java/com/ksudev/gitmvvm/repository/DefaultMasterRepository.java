package com.ksudev.gitmvvm.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.ksudev.gitmvvm.api.ApiFactory;
import com.ksudev.gitmvvm.content.translate.Language;
import com.ksudev.gitmvvm.content.translate.Translation;
import com.ksudev.gitmvvm.utils.LanguageUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class DefaultMasterRepository implements MasterRepository {
    
    @NonNull
    @Override
    public Observable<List<Translation>> getTranslate() {
        Log.e("MasterRepository", "send request getTranslate");
        return ApiFactory.getMasterService()
                .loadTranslate()
                .flatMap((Function<Response<Object>, ObservableSource<List<Translation>>>) response -> {
                    String bodyStr = new Gson().toJson(response.body());
                    List<Translation> list = new ArrayList<>();
                    try {
                        list = new ArrayList<>(DefaultMasterRepository.this.parse(bodyStr));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (list.isEmpty()) {
                        return Observable.just(DefaultMasterRepository.this.getTranslateFromDb());
                    } else {
                        if (!DefaultMasterRepository.this.isFromCache(response)) {
                            DefaultMasterRepository.this.saveTranslate(list);
                        }
                        return Observable.just(list);
                    }
                })
                .onErrorResumeNext(throwable -> {
                    Log.e("getTranslate", "error", throwable);
                    return Observable.just(getTranslateFromDb());
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    
    private List<Translation> getTranslateFromDb() {
        return new ArrayList<>();
    }
    
    private void saveTranslate(List<Translation> translationList) {
        Log.e("MasterRepository", "saveTranslate = " + translationList.size());
    }
    
    private boolean isFromCache(Response response) {
        return response.raw().networkResponse() != null && response.raw().networkResponse().code() >= 300;
    }
    
    private List<Translation> parse(String response) throws JSONException {
        List<Translation> list = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray names = jsonObject.names();
        JSONArray languages = jsonObject.toJSONArray(names);
        LanguageUtils languageUtils = new LanguageUtils();
        for (int i = 0; i < names.length(); i++) {
            String lngCode = names.getString(i);
            Language language = languageUtils.getLanguageByLngCode(lngCode);
            languageUtils.createOrUpdate(language);
            JSONObject lngObj = languages.optJSONObject(i);
            JSONArray keys = lngObj.names();
            JSONArray values = lngObj.toJSONArray(keys);
            for (int k = 0; k < keys.length(); k++) {
                String key = keys.getString(k);
                String value = values.getString(k).replaceAll("\\\\n", "\n");
                list.add(new Translation(key, lngCode, value));
            }
        }
        return list;
    }
}
