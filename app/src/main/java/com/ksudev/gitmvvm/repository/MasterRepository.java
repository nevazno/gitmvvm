package com.ksudev.gitmvvm.repository;

import androidx.annotation.NonNull;

import com.ksudev.gitmvvm.content.translate.Translation;

import java.util.List;

import io.reactivex.Observable;

public interface MasterRepository {
    
    @NonNull
    Observable<List<Translation>> getTranslate();
}
