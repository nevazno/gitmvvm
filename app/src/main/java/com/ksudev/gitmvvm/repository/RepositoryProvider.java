package com.ksudev.gitmvvm.repository;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

/**
 * @author nevazno
 */
public final class RepositoryProvider {

    private static GithubRepository sGithubRepository;
    private static MasterRepository sMasterRepository;

    private RepositoryProvider() {
    }

    @NonNull
    public static GithubRepository provideGithubRepository() {
        if (sGithubRepository == null) {
            sGithubRepository = new DefaultGithubRepository();
        }
        return sGithubRepository;
    }

    @NonNull
    public static MasterRepository provideMasterRepository() {
        if (sMasterRepository == null) {
            sMasterRepository = new DefaultMasterRepository();
        }
        return sMasterRepository;
    }

    public static void setGithubRepository(@NonNull GithubRepository githubRepository) {
        sGithubRepository = githubRepository;
    }

    @MainThread
    public static void init() {
        sGithubRepository = new DefaultGithubRepository();
        sMasterRepository = new DefaultMasterRepository();
    }
}
