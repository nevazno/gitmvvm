package com.ksudev.gitmvvm.repository;

import androidx.annotation.NonNull;

import com.ksudev.gitmvvm.content.Authorization;
import com.ksudev.gitmvvm.content.Repository;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author nevazno
 */
public interface GithubRepository {
    
    @NonNull
    Observable<List<Repository>> repositories();
    
    @NonNull
    Observable<Authorization> auth(@NonNull String login, @NonNull String password);
}
