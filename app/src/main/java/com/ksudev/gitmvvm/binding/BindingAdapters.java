package com.ksudev.gitmvvm.binding;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ksudev.gitmvvm.AppDelegate;
import com.ksudev.gitmvvm.widget.MaterialEditText;

@SuppressWarnings("unused")
@InverseBindingMethods({
        @InverseBindingMethod(type = MaterialEditText.class, attribute = "edit:text"),
        @InverseBindingMethod(type = MaterialEditText.class, attribute = "edit:error"),
})

public class BindingAdapters {
    
    private BindingAdapters() {
        throw new AssertionError();
    }
    
    @BindingAdapter("app:onClick")
    public static void bindOnClick(View view, final Runnable runnable) {
        view.setOnClickListener(v -> runnable.run());
    }
    
    @BindingAdapter("app:text")
    public static void bindSetText(TextView view, String key) {
        AppDelegate.getTranslateManager().getTranslateObs().subscribe(s -> view.setText(AppDelegate.getTranslateManager().getTranslate(key)));
    }
    
    @BindingAdapter("app:hint")
    public static void bindSetHint(TextInputEditText view, String key) {
        AppDelegate.getTranslateManager().getTranslateObs().subscribe(s -> view.setHint(AppDelegate.getTranslateManager().getTranslate(key)));
    }
    
    @BindingAdapter("edit:hint")
    public static void bindETHint(MaterialEditText view, String key) {
        AppDelegate.getTranslateManager().getTranslateObs().subscribe(s -> view.getEditText().setHint(AppDelegate.getTranslateManager().getTranslate(key)));
    }
    
    @BindingAdapter("app:layoutError")
    public static void bindEditError(TextInputLayout view, String error) {
        Log.e("BindingAdapters", "app:layoutError error = " + error);
        view.setError(error);
    }
    
    @BindingAdapter("app:onFocusChange")
    public static void onFocusChange(View view, final View.OnFocusChangeListener listener) {
        view.setOnFocusChangeListener(listener);
    }
    
    @BindingAdapter("edit:error")
    public static void setError(MaterialEditText view, String error) {
        Log.e("BindingAdapters", "setError = " + error);
        if (error != null) {
            view.getErrorChangeDetector().onNext(error);
        }
    }
    
    @InverseBindingAdapter(attribute = "edit:error")
    public static String getError(MaterialEditText view) {
        String error = view.getError() != null ? view.getError() : "";
        Log.e("BindingAdapters", "getError return = " + error);
        return error;
    }
    
    @BindingAdapter(value = "errorAttrChanged")
    public static void setErrorListener(MaterialEditText materialEditText, final InverseBindingListener textAttrChanged) {
        Log.e("BindingAdapters", "errorAttrChanged");
        if (textAttrChanged != null) {
            materialEditText.getErrorChangeDetector()
                    .subscribe(s -> {
                        Log.e("BindingAdapters", "errorAttrChanged str = " + s);
                        textAttrChanged.onChange();
                    });
        }
    }
    
    @BindingAdapter("edit:onFocusChange")
    public static void onFocusChange(MaterialEditText view, View.OnFocusChangeListener listener) {
        View.OnFocusChangeListener focusChangeListener = (v, hasFocus) -> {
            view.setFocused(hasFocus);
            listener.onFocusChange(v, hasFocus);
        };
        view.getEditText().setOnFocusChangeListener(focusChangeListener);
    }
    
    @BindingAdapter("edit:text")
    public static void setText(MaterialEditText view, String value) {
        if (value != null && !value.equals(getText(view)))
            view.getEditText().setText(value);
    }
    
    @InverseBindingAdapter(attribute = "edit:text")
    public static String getText(MaterialEditText view) {
        return view.getEditText().getText() != null ? view.getEditText().getText().toString() : "";
    }
    
    @BindingAdapter(value = "textAttrChanged")
    public static void setTextListener(MaterialEditText materialEditText, final InverseBindingListener textAttrChanged) {
        if (textAttrChanged != null) {
            materialEditText.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                
                }
                
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                
                }
                
                @Override
                public void afterTextChanged(Editable editable) {
                    textAttrChanged.onChange();
                }
            });
        }
    }
    
}