package com.ksudev.gitmvvm.binding;

import android.util.Log;
import android.view.View;


public class Handler {
    public View.OnFocusChangeListener getOnFocusChangeListener() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocused) {
                Log.e("Handler", "onFocusChange");
                Log.e("Handler", "isFocused = " + isFocused);
//                ((TextInputEditText)view).setFocused(isFocused);
            }
        };
    }
}
