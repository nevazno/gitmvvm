package com.ksudev.gitmvvm.binding;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.IntRange;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.databinding.InverseMethod;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.android.material.textfield.TextInputLayout;
import com.ksudev.gitmvvm.drawables.DisplayDrawable;
import com.ksudev.gitmvvm.widget.MaterialEditText;


public class InputFieldComponent {
    @BindingAdapter({"error"})
    public static void setError(TextInputLayout inputLayout, ObservableField<CharSequence> errorText) {
        inputLayout.setError(errorText.get());
    }
    
    @BindingAdapter({"hintColor", "errorColor"})
    public static void bind(TextInputLayout inputLayout, ObservableInt hintColor, ObservableInt errorColor) {
        inputLayout.setErrorTextColor(ColorStateList.valueOf(errorColor.get()));
        inputLayout.setDefaultHintTextColor(ColorStateList.valueOf(hintColor.get()));
    }
    
    @BindingAdapter("onFocusChange")
    public static void setOnFocusChangeListener(View view, View.OnFocusChangeListener listener) {
        view.setOnFocusChangeListener(listener);
    }
    
    @BindingAdapter("inputType")
    public static void setRawInputType(TextInputLayout inputLayout, int type) {
        EditText editText = inputLayout.getEditText();
        if (editText != null) {
            editText.setRawInputType(type);
        }
    }
    
    
    @BindingAdapter("requestFocus")
    public static void requestFocus(EditText editText, ObservableBoolean requestFocus) {
        if (requestFocus.get()) {
            editText.requestFocus();
            editText.setSelection(editText.getText().length());
//            Drawable background = editText.getBackground();
//            if (background instanceof UnderlineDrawable) {
//                ((UnderlineInsetDrawable) background).start();
//            }
        
        }
        
    }
    
    
}
