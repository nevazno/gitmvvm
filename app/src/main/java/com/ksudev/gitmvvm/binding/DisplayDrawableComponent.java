package com.ksudev.gitmvvm.binding;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;

import com.ksudev.gitmvvm.drawables.DisplayDrawable;

@InverseBindingMethods({
        @InverseBindingMethod(type = ImageView.class, attribute = "onLevelChanged", event = "levelAttrChanged"),
        @InverseBindingMethod(type = ImageView.class, attribute = "android:background"),
})
public class DisplayDrawableComponent {
    
    @InverseBindingAdapter(attribute = "level", event = "levelAttrChanged")
    public static int getLevel(ImageView view) {
        Drawable drawable = view.getDrawable();
        if (drawable instanceof DisplayDrawable) {
            DisplayDrawable displayDrawable = (DisplayDrawable) drawable;
            return displayDrawable.getLevel();
        } else {
            return 0;
        }
    }
    
    @BindingAdapter({"level", "android:background"})
    public static void setLevel(ImageView view, int level, DisplayDrawable drawable) {
        view.setImageDrawable(drawable);
        if (drawable != null) {
            if (drawable.getLevel() != level) {
                drawable.setLevel(level);
            }
        }
    }
    
    
    @BindingAdapter(value = {"onLevelChanged", "levelAttrChanged"}, requireAll = false)
    public static void setOnLevelChangedListener(ImageView view, OnLevelChangedListener onLevelChangedListener, InverseBindingListener levelAttrChangedListener) {
        Drawable drawable = view.getDrawable();
        if (drawable instanceof DisplayDrawable) {
            DisplayDrawable displayDrawable = (DisplayDrawable) drawable;
            if (onLevelChangedListener == null && levelAttrChangedListener == null) {
                displayDrawable.setBackgroundLevelChangedListener(null);
            } else {
                displayDrawable.setBackgroundLevelChangedListener(level -> {
                    if (onLevelChangedListener != null) {
                        onLevelChangedListener.onBackgroundLevelChanged(level);
                    }
                    if (levelAttrChangedListener != null) {
                        levelAttrChangedListener.onChange();
                    }
                });
            }
        }
    }
    
    public interface OnLevelChangedListener {
        void onBackgroundLevelChanged(int level);
    }
    
}
