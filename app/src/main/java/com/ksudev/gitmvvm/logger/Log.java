/*
 * Copyright UWhiz (c) 2019.
 */

package com.ksudev.gitmvvm.logger;

import androidx.annotation.NonNull;

import com.ksudev.gitmvvm.BuildConfig;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import static android.util.Log.DEBUG;
import static android.util.Log.ERROR;
import static android.util.Log.INFO;
import static android.util.Log.VERBOSE;
import static android.util.Log.WARN;
import static com.ksudev.gitmvvm.logger.StackTraceInfo.CURRENT_CLASS_NAME;
import static com.ksudev.gitmvvm.logger.StackTraceInfo.CURRENT_LINE_NUMBER;
import static com.ksudev.gitmvvm.logger.StackTraceInfo.CURRENT_METHOD_NAME;
import static com.ksudev.gitmvvm.logger.StackTraceInfo.INVOKED_CLASS_NAME;
import static com.ksudev.gitmvvm.logger.StackTraceInfo.INVOKED_LINE_NUMBER;
import static com.ksudev.gitmvvm.logger.StackTraceInfo.INVOKED_METHOD_NAME;

@SuppressWarnings({"unused"})
public class Log {
    private static boolean isLoggingEnabled = BuildConfig.DEBUG;
    private static Set<String> ignoredLoggingClasses = new ConcurrentSkipListSet<>();
    
    private Log() {
        //init
    }
    
    public static <T> void ignoreLoggingClass(@NonNull T classInstance) {
        innerIgnoreLoggingClass(classInstance.getClass(), getLogData());
    }
    
    public static void ignoreLoggingClass(@NonNull Class aClass) {
        innerIgnoreLoggingClass(aClass, getLogData());
    }
    
    private static <T> void innerIgnoreLoggingClass(@NonNull Class aClass, String[] logData) {
        String className = aClass.getSimpleName();
        String tag = getTag(className);
        if (logData[0].equalsIgnoreCase(tag)) {
            ignoredLoggingClasses.add(tag);
        }
    }
    
    private static int println(int priority, String[] logData, String msg) {
        String tag = logData[0];
        return isLoggingEnabled(tag) ? android.util.Log.println(priority, tag, logData[1] + msg) : -1;
    }
    
    private static boolean isLoggingEnabled(String tag) {
        return isLoggingEnabled & !ignoredLoggingClasses.contains(tag);
    }
    
    @NonNull
    private static String[] getLogData() {
        String[] logData = StackTraceInfo.getCurrentLogData(2);
        String className = logData[CURRENT_CLASS_NAME];
        String simpleClassName = className.substring(className.lastIndexOf(".") + 1);
        String tag = getTag(simpleClassName);
        
        String lineCode = logData[CURRENT_LINE_NUMBER];
        
        String currentMethodName = logData[CURRENT_METHOD_NAME];
        String invokedClassName = logData[INVOKED_CLASS_NAME];
        String invokedMethodName = logData[INVOKED_METHOD_NAME];
        String invokedLineCode = logData[INVOKED_LINE_NUMBER];
        String formattedLogPrefix = "<";
        if (!className.equals(invokedClassName)) {
            String invokedSimpleClassName = invokedClassName.substring(invokedClassName.lastIndexOf(".") + 1);
            formattedLogPrefix += invokedSimpleClassName + ".";
        }
        formattedLogPrefix += invokedMethodName + ":" + invokedLineCode + " -> " + currentMethodName + ":" + lineCode + "> ";
        return new String[]{tag, formattedLogPrefix};
    }
    
    private static String getTag(String simpleClassName) {
        return "[" + simpleClassName + "] ";
    }
    
    /**
     * @deprecated
     */
    public static int v(String tag, String msg) {
        String[] logData = getLogData();
        return println(INFO, logData, msg);
    }
    
    public static int v(String msg) {
        String[] logData = getLogData();
        return println(VERBOSE, logData, msg);
    }
    
    /**
     * @deprecated
     */
    public static int d(String tag, String msg) {
        String[] logData = getLogData();
        return println(INFO, logData, msg);
    }
    
    public static int d(String msg) {
        String[] logData = getLogData();
        return println(DEBUG, logData, msg);
    }
    
    /**
     * @deprecated
     */
    public static int i(String tag, String msg) {
        String[] logData = getLogData();
        return println(INFO, logData, msg);
    }
    
    public static int i(String msg) {
        String[] logData = getLogData();
        return println(INFO, logData, msg);
    }
    
    public static int w(String msg) {
        String[] logData = getLogData();
        return println(WARN, logData, msg);
    }
    
    /**
     * @deprecated
     */
    public static int w(String tag, String msg) {
        String[] logData = getLogData();
        return println(WARN, logData, msg);
    }
    
    /**
     * @deprecated
     */
    public static int w(String tag, String msg, Throwable tr) {
        String[] logData = getLogData();
        String tag1 = logData[0];
        return isLoggingEnabled(tag1) ? android.util.Log.w(tag1, logData[1] + msg, tr) : -1;
    }
    
    public static int w(String msg, Throwable tr) {
        String[] logData = getLogData();
        String tag = logData[0];
        return isLoggingEnabled(tag) ? android.util.Log.w(tag, logData[1] + msg, tr) : -1;
    }
    
    public static int e(String msg, Throwable tr) {
        String[] logData = getLogData();
        String tag = logData[0];
        return isLoggingEnabled(tag) ? android.util.Log.e(tag, logData[1] + msg, tr) : -1;
    }
    
    /**
     * @deprecated
     */
    public static int e(String tag, String msg) {
        String[] logData = getLogData();
        return println(ERROR, logData, msg);
    }
    
    public static int e(String msg) {
        String[] logData = getLogData();
        return println(ERROR, logData, msg);
    }
    
    /**
     * @deprecated
     */
    public static int e(String tag, String msg, Throwable tr) {
        String[] logData = getLogData();
        String tag1 = logData[0];
        return isLoggingEnabled(tag1) ? android.util.Log.e(tag1, logData[1] + msg, tr) : -1;
    }
    
    public static int e(Exception e) {
        String[] logData = getLogData();
        String tag = logData[0];
        return isLoggingEnabled(tag) ? android.util.Log.e(tag, logData[1] + e.getMessage(), e.getCause()) : -1;
    }
}
