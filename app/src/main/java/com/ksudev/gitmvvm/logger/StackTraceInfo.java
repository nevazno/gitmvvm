/*
 * Copyright UWhiz (c) 2019.
 */

package com.ksudev.gitmvvm.logger;

import java.text.MessageFormat;

@SuppressWarnings({"unused", "SameParameterValue", "WeakerAccess"})
public class StackTraceInfo {
    public static final int CURRENT_CLASS_NAME = 0;
    public static final int CURRENT_METHOD_NAME = 1;
    public static final int CURRENT_LINE_NUMBER = 2;
    public static final int INVOKED_CLASS_NAME = 3;
    public static final int INVOKED_METHOD_NAME = 4;
    public static final int INVOKED_LINE_NUMBER = 5;
    private static int CLIENT_CODE_STACK_INDEX;
    static String invokingFileNameFqn = MessageFormat.format("{0}({1})", getInvokingMethodNameFqn(2), getInvokingFileName(2));
    
    static {
        // Finds out the index of "this code" in the returned stack trace -
        // funny but it differs in JDK 1.5 and 1.6
        int i = 0;
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            i++;
            if (ste.getClassName().equalsIgnoreCase(StackTraceInfo.class.getName())) {
                break;
            }
        }
        CLIENT_CODE_STACK_INDEX = i;
    }
    
    static String currentFileNameFqn() {
        String currentMethodNameFqn = getCurrentMethodNameFqn(1);
        String currentFileName = getCurrentFileName(1);
        return MessageFormat.format("{0}({1})", currentMethodNameFqn, currentFileName);
    }
    
    public static String getCurrentMethodName(int offset) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        return stackTrace[CLIENT_CODE_STACK_INDEX + offset].getMethodName();
    }
    
    public static String getCurrentClassName(int offset) {
        return Thread.currentThread().getStackTrace()[CLIENT_CODE_STACK_INDEX + offset].getClassName();
    }
    
    public static String getCurrentFileName(int offset) {
        String filename = Thread.currentThread().getStackTrace()[CLIENT_CODE_STACK_INDEX + offset].getFileName();
        int lineNumber = Thread.currentThread().getStackTrace()[CLIENT_CODE_STACK_INDEX + offset].getLineNumber();
        return MessageFormat.format("{0}:{1}", filename, lineNumber);
    }
    
    public static int getLineNumber(int offset) {
        return Thread.currentThread().getStackTrace()[CLIENT_CODE_STACK_INDEX + offset].getLineNumber();
    }
    
    public static String[] getCurrentLogData(int offset) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        StackTraceElement stackTraceElement = stackTrace[CLIENT_CODE_STACK_INDEX + offset];
        StackTraceElement prevStackTraceElement = stackTrace[CLIENT_CODE_STACK_INDEX + offset + 1];
        
        String currentClassName = stackTraceElement.getClassName();
        String currentMethodName = stackTraceElement.getMethodName();
        String currentLineNumber = String.valueOf(stackTraceElement.getLineNumber());
        
        String invokedLineNumber = String.valueOf(prevStackTraceElement.getLineNumber());
        String invokedMethodName = prevStackTraceElement.getMethodName();
        String invokedClassName = prevStackTraceElement.getClassName();
        return new String[]{currentClassName, currentMethodName, currentLineNumber, invokedClassName, invokedMethodName, invokedLineNumber};
    }
    
    public static String getInvokingMethodName(int offset) {
        return getCurrentMethodName(offset + 1); // re-uses
        // getCurrentMethodName() with
        // desired index
    }
    
    public static String getInvokingClassName(int offset) {
        return getCurrentClassName(offset + 1); // re-uses getCurrentClassName()
        // with desired index
    }
    
    public static String getInvokingFileName(int offset) {
        return getCurrentFileName(offset + 1); // re-uses getCurrentFileName()
        // with desired index
    }
    
    public static String getCurrentMethodNameFqn(int offset) {
        String currentClassName = getCurrentClassName(offset + 1);
        String currentMethodName = getCurrentMethodName(offset + 1);
        return MessageFormat.format("{0}.{1}", currentClassName, currentMethodName);
    }
    
    public static String getInvokingMethodNameFqn(int offset) {
        String invokingClassName = getInvokingClassName(offset + 1);
        String invokingMethodName = getInvokingMethodName(offset + 1);
        return MessageFormat.format("{0}.{1}", invokingClassName, invokingMethodName);
    }
}
