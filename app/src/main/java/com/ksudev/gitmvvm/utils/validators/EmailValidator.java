package com.ksudev.gitmvvm.utils.validators;


/**
 * Created by victor on 10/1/14.
 */
@SuppressWarnings("WeakerAccess")
public class EmailValidator extends RegularExpressionValidator {
    
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*+(\\+[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,6})$";
    
    public EmailValidator() {
        super(EMAIL_PATTERN, "Error!! : %s");
    }
}
