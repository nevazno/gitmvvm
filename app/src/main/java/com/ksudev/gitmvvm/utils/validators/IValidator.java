package com.ksudev.gitmvvm.utils.validators;

/**
 * Created by victor on 10/1/14.
 */
public interface IValidator {
    boolean isValid(CharSequence value);
    String getErrorMessage(CharSequence value);
}
