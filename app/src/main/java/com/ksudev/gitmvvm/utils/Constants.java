package com.ksudev.gitmvvm.utils;

public class Constants {
    public static final String LOGIN = "login.field.username";
    public static final String PASS = "login.field.password";
    public static final String REMEMBER = "login.header.remember";
    public static final String LOG_IN = "login.button.login";
    public static final String LNG = "LNG";
}
