package com.ksudev.gitmvvm.utils.validators;

import android.text.TextUtils;

import java.util.Locale;

/**
 * Created by victor on 10/1/14.
 */
@SuppressWarnings("WeakerAccess")
public abstract class RegularExpressionValidator implements IValidator {
    private final String regularExpression;
    private final String message;
    private String errorMessage;
    
    public RegularExpressionValidator(String regularExpression, String message) {
        this.regularExpression = regularExpression;
        this.message = message;
    }
    
    @Override
    public boolean isValid(CharSequence value) {
        boolean result = true;
        if(!TextUtils.isEmpty(value)){
            result = String.valueOf(value).matches(regularExpression);
        }
        errorMessage = null;
        if (!result) {
            errorMessage = message;
        }
        return result;
    }
    
    @Override
    public String getErrorMessage(CharSequence value) {
        return String.format(Locale.getDefault(), errorMessage, value);
    }
}
