package com.ksudev.gitmvvm.utils.animation;

import android.util.Property;

@SuppressWarnings({"unused"})
public abstract class IntProperty<T> extends Property<T, Integer> {
    
    public IntProperty(String name) {
        super(Integer.class, name);
    }
    
    public abstract void setValue(T object, int value);
    
    @Override
    final public void set(T object, Integer value) {
        setValue(object, value);
    }
    
}