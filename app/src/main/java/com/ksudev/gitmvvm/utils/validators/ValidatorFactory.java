package com.ksudev.gitmvvm.utils.validators;

/**
 * Created by victor on 10/1/14.
 */
public class ValidatorFactory {
    
    public static IValidator validatorInstance(String validatorType) {
        IValidator validator = null;
        if (ValidatorType.EMAIL.getValue().equals(validatorType)) {
            validator = new EmailValidator();
        }
        return validator;
    }
    
    public enum ValidatorType {
        EMAIL("email"),
        STATE("state"),
        DATE("date");
        
        private final String value;
        
        ValidatorType(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
    
}
