package com.ksudev.gitmvvm.utils;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.ksudev.gitmvvm.content.translate.Language;

import java.util.Locale;

public class LanguageUtils {

    public Language getLanguageByLngCode(@Nullable String lngCode) {
        if (TextUtils.isEmpty(lngCode)) {
            return null;
        }
        Locale[] availableLocales = Locale.getAvailableLocales();
        for (Locale locale : availableLocales) {
            if (locale.getLanguage().equals(lngCode)) {
                return new Language(locale);
            }
        }
        return null;
    }

    public void createOrUpdate(Language language) {
    }
}
