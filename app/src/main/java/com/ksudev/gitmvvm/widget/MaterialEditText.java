package com.ksudev.gitmvvm.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ksudev.gitmvvm.R;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;

public class MaterialEditText extends RelativeLayout {
    
    private TextInputLayout textInputLayout;
    private TextInputEditText editText;
    
    private String error;
    
    private BehaviorSubject<String> textErrorChangeDetector;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    
    public MaterialEditText(Context context) {
        super(context);
        init(context);
    }
    
    public MaterialEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }
    
    public MaterialEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    
    public TextInputLayout getTextInputLayout() {
        return textInputLayout;
    }
    
    public TextInputEditText getEditText() {
        return editText;
    }
    
    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.material_floating_hint_edit_text, this);
        this.textInputLayout = findViewById(R.id.text_input_layout);
        this.editText = findViewById(R.id.floating_edit_text);
        textErrorChangeDetector = BehaviorSubject.create();
        Disposable disposable = textErrorChangeDetector.subscribe(message -> {
            error = message;
            getTextInputLayout().setError(message);
        });
        compositeDisposable.add(disposable);
    }
    
    public void setFocused(boolean isFocused) {
        Log.e("MaterialEditText", "setFocused = " + isFocused);
        if (isFocused) {
            getErrorChangeDetector().onNext("");
        } else {
            if (getEditText().getText() == null || TextUtils.isEmpty(getEditText().getText().toString())) {
                getErrorChangeDetector().onNext("Field is empty, send from MaterialEditText");
            }
        }
    }
    
    public BehaviorSubject<String> getErrorChangeDetector() {
        return textErrorChangeDetector;
    }
    
    public String getError() {
        return error;
    }
}
