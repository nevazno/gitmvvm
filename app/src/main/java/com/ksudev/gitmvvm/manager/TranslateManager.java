package com.ksudev.gitmvvm.manager;

import android.util.Log;

import com.ksudev.gitmvvm.content.translate.Translation;
import com.ksudev.gitmvvm.repository.RepositoryProvider;
import com.ksudev.gitmvvm.utils.Constants;
import com.orhanobut.hawk.Hawk;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class TranslateManager {
    
    private static Map<String, List<Translation>> mapTranslation;
    private static BehaviorSubject<String> detector;
    private PublishSubject<String> translateObs;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    
    private void initTranslateObs() {
        translateObs = PublishSubject.create();
        Disposable disposableObs = translateObs
                .throttleFirst(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    Log.e("TranslateManager", "s = " + s);
                    Disposable disposableTranslate = RepositoryProvider.provideMasterRepository()
                            .getTranslate()
                            .retry(3)
//                                .doOnSubscribe(() -> setLoading(true))
//                                .doOnTerminate(() -> setLoading(false))
                            .subscribe(TranslateManager.this::setResponse, TranslateManager.this::showError);
                    compositeDisposable.add(disposableTranslate);
                });
        compositeDisposable.add(disposableObs);
    }
    
    public void init() {
        Log.e("TranslateManager", "init");
        detector = BehaviorSubject.create();
        detector.onNext(getLng());
        
        initTranslateObs();

//        List<Translation> translations = new LinkedList<>();
//        translations.add(new Translation(Constants.LOGIN, "en", "Login"));
//        translations.add(new Translation(Constants.PASS, "en", "Password"));
//        translations.add(new Translation(Constants.REMEMBER, "en", "Remember me on this device"));
//        translations.add(new Translation(Constants.LOG_IN, "en", "LOGIN"));
//
//        translations.add(new Translation(Constants.LOGIN, "ru", "Логин"));
//        translations.add(new Translation(Constants.PASS, "ru", "Пароль"));
//        translations.add(new Translation(Constants.REMEMBER, "ru", "Помнить меня на этом устройстве"));
//        translations.add(new Translation(Constants.LOG_IN, "ru", "Войти"));
//        setResponse(translations);
        
        getTranslate();
    }
    
    private void getTranslate() {
        Log.e("TranslateManager", "getTranslate");
        translateObs.onNext("s");
    }
    
    private void setResponse(List<Translation> translations) {
        Log.e("TranslateManager", "setResponse, translations size = " + translations.size());
        mapTranslation = createMapTranslation(translations);
        setLng(detector.getValue());
    }
    
    private void showError(Throwable throwable) {
        Log.e("TranslateManager", "error", throwable);
    }
    
    private Map<String, List<Translation>> createMapTranslation(List<Translation> translations) {
        Map<String, List<Translation>> mapTranslation = new HashMap<>();
        HashSet<String> setLanguageKeys = getLngCodeList(translations);
        for (String lngCode : setLanguageKeys) {
            List<Translation> translationTempList = new LinkedList<>();
            for (Translation tr : translations) {
                if (tr.getLngCode().equalsIgnoreCase(lngCode))
                    translationTempList.add(tr);
            }
            mapTranslation.put(lngCode, translationTempList);
        }
        return mapTranslation;
    }
    
    private HashSet<String> getLngCodeList(List<Translation> translations) {
        HashSet<String> setLanguageKeys = new HashSet<>();
        for (Translation tr : translations) {
            setLanguageKeys.add(tr.getLngCode());
        }
        return setLanguageKeys;
    }
    
    public String getLng() {
        String lng = Hawk.get(Constants.LNG, "en");
        Log.e("TranslateManager", "getLng = " + lng);
        return lng;
    }
    
    public void setLng(String lng) {
        getTranslateObs().onNext(lng);
        Hawk.put(Constants.LNG, lng);
    }
    
    public String getTranslate(String key) {
        Log.e("TranslateManager", "getTranslate by key = " + key);
        if (mapTranslation == null || mapTranslation.isEmpty()) {
            getTranslate();
        } else {
            List<Translation> translationList = mapTranslation.get(detector.getValue());
            if (translationList != null) {
                for (int i = 0; i < translationList.size(); i++) {
                    if (translationList.get(i).getKey().equalsIgnoreCase(key)) {
                        return translationList.get(i).getValue();
                    }
                }
            }
        }
        return "no translate";
    }
    
    public BehaviorSubject<String> getTranslateObs() {
        return TranslateManager.detector;
    }
    
    public void onDestroy() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
