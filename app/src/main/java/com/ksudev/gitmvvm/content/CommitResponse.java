package com.ksudev.gitmvvm.content;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * @author nevazno
 */
public class CommitResponse {

    @SerializedName("commit")
    private Commit mCommit;

    @NonNull
    public Commit getCommit() {
        return mCommit;
    }
}