package com.ksudev.gitmvvm.content.translate;

import org.apache.commons.lang3.text.WordUtils;

import java.util.Locale;

public class Language {

    private String code;
    private String name;

    public Language() {
    }

    public Language(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public Language(Locale locale) {
        this(locale.getLanguage(), WordUtils.capitalize(locale.getDisplayLanguage()));
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
