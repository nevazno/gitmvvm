package com.ksudev.gitmvvm.content.translate;

public class Translation {
    private String key;
    private String lngCode;
    private String value;

    public Translation(String key, String lngCode, String value) {
        this.key = key;
        this.lngCode = lngCode;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getLngCode() {
        return lngCode;
    }

}
