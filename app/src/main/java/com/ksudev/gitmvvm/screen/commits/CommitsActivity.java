package com.ksudev.gitmvvm.screen.commits;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import androidx.databinding.library.baseAdapters.BR;
import com.ksudev.gitmvvm.R;
import com.ksudev.gitmvvm.content.Repository;
import com.ksudev.gitmvvm.databinding.ActivityCommitsBinding;
import com.ksudev.gitmvvm.widget.EmptyRecyclerView;
import com.stfalcon.androidmvvmhelper.mvvm.activities.BindingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author nevazno
 */
public class CommitsActivity extends BindingActivity<ActivityCommitsBinding, CommitsActivityVM> {

    private static final String REPO_NAME_KEY = "repo_name_key";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.recyclerView)
    EmptyRecyclerView mRecyclerView;

    public static void start(@NonNull Activity activity, @NonNull Repository repository) {
        Intent intent = new Intent(activity, CommitsActivity.class);
        intent.putExtra(REPO_NAME_KEY, repository.getName());
        activity.startActivity(intent);
    }

    @Override
    public CommitsActivityVM onCreate() {
        ButterKnife.bind(this);
        String repositoryName = getIntent().getStringExtra(REPO_NAME_KEY);
        return new CommitsActivityVM(this, repositoryName);
    }

    @Override
    public int getVariable() {
        return BR.commitsViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_commits;
    }
}
