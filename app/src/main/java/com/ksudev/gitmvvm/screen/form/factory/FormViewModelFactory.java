package com.ksudev.gitmvvm.screen.form.factory;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.ksudev.gitmvvm.logger.Log;
import com.ksudev.gitmvvm.screen.form.FormActivity;
import com.ksudev.gitmvvm.screen.form.models.InputFieldViewModel;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("WeakerAccess")
public class FormViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    
    private static FormViewModelFactory instance = null;
    private WeakReference<FragmentActivity> activityRef;
    
    public FormViewModelFactory(FormActivity formActivity) {
        activityRef = new WeakReference<>(formActivity);
    }
    
    @NonNull
    public static synchronized FormViewModelFactory getInstance(FormActivity formActivity) {
        if (instance == null) {
            instance = new FormViewModelFactory(formActivity);
        }
        return instance;
    }
    
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        String keyPrefix = modelClass.getCanonicalName() + InputFieldViewModel.class.getCanonicalName();
//        InputFieldViewModel loginField = ViewModelProviders.of(activityRef.get()).get(keyPrefix + "Login", InputFieldViewModel.class);
        InputFieldViewModel loginField = new InputFieldViewModel();
        InputFieldViewModel passField = new InputFieldViewModel();
//        InputFieldViewModel passField = ViewModelProviders.of(activityRef.get()).get(keyPrefix + "Pass", InputFieldViewModel.class);
       
        
        
        try {
            return modelClass.getConstructor(InputFieldViewModel.class, InputFieldViewModel.class).newInstance(loginField, passField);
        } catch (IllegalAccessException e) {
            Log.e(e);
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            Log.e(e);
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            Log.e(e);
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            Log.e(e);
            throw new RuntimeException(e);
        }
    }
}
