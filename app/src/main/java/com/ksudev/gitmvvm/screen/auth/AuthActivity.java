package com.ksudev.gitmvvm.screen.auth;

import android.app.Activity;
import android.content.Intent;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BR;

import com.ksudev.gitmvvm.R;
import com.ksudev.gitmvvm.databinding.ActivityAuthBinding;
import com.stfalcon.androidmvvmhelper.mvvm.activities.BindingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthActivity extends BindingActivity<ActivityAuthBinding, AuthActivityVM> {
    
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    
    @BindView(R.id.radioButtonEn)
    RadioButton radioButtonEn;
    
    @BindView(R.id.radioButtonRu)
    RadioButton radioButtonRu;
    
    public static void start(@NonNull Activity activity) {
        Intent intent = new Intent(activity, AuthActivity.class);
        activity.startActivity(intent);
    }
    
    
    @Override
    public AuthActivityVM onCreate() {
        ButterKnife.bind(this);
        return new AuthActivityVM(this);
    }
    
    @Override
    public int getVariable() {
        return BR.authViewModel;
    }
    
    @Override
    public int getLayoutId() {
        return R.layout.activity_auth;
    }
}
