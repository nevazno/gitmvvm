package com.ksudev.gitmvvm.screen.general;

/**
 * @author nevazno
 */
public interface LoadingView {

    void showLoading();

    void hideLoading();

}