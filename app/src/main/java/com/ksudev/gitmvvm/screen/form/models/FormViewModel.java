package com.ksudev.gitmvvm.screen.form.models;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.databinding.adapters.SeekBarBindingAdapter;
import androidx.lifecycle.ViewModel;

import com.ksudev.gitmvvm.binding.DisplayDrawableComponent;
import com.ksudev.gitmvvm.drawables.UnderlineDrawable;
import com.ksudev.gitmvvm.logger.Log;
import com.ksudev.gitmvvm.utils.validators.EmailValidator;

import java.util.Locale;

public class FormViewModel extends ViewModel {
    private final static String FORMAT_LABEL = "%.2f %%";
    private InputFieldViewModel loginField;
    private InputFieldViewModel passField;
    private SeekBarBindingAdapter.OnProgressChanged seekBarChangeListener;
    private DisplayDrawableComponent.OnLevelChangedListener onLevelChanged;
    private ObservableInt level = new ObservableInt(0);
    private ObservableField<String> seekBarFormattedValue = new ObservableField<>(seekBarValueFormat(0f));
    private ObservableBoolean drawableToolsButtonChecked = new ObservableBoolean(false);
    
    public FormViewModel(InputFieldViewModel loginField, InputFieldViewModel passField) {
        this.loginField = loginField;
        this.passField = passField;
        initParams();
    }
    
    public ObservableInt getLevel() {
        return level;
    }
    
    public ObservableField<String> getSeekBarFormattedValue() {
        return seekBarFormattedValue;
    }
    
    private String seekBarValueFormat(float progress) {
        return String.format(Locale.getDefault(), FORMAT_LABEL, progress);
    }
    
    public DisplayDrawableComponent.OnLevelChangedListener getOnLevelChanged() {
        if (onLevelChanged == null) {
            onLevelChanged = level -> FormViewModel.this.level.set(level);
        }
        return onLevelChanged;
    }
    
    public SeekBarBindingAdapter.OnProgressChanged getSeekBarChangeListener() {
        if (seekBarChangeListener == null) {
            seekBarChangeListener = (seekBar, level, fromUser) -> {
                FormViewModel.this.level.set(level);
                seekBarFormattedValue.set(seekBarValueFormat(level * 100f / UnderlineDrawable.MAX_LEVEL));
            };
        }
        return seekBarChangeListener;
    }
    
    public ObservableBoolean getDrawableToolsButtonChecked() {
        return drawableToolsButtonChecked;
    }
    
    
    private void initParams() {
        loginField.setValidator(new EmailValidator());
        loginField.setHint("Login");
        passField.setValidator(new EmailValidator());
        passField.setHint("Password");
    }
    
    public InputFieldViewModel getLoginField() {
        return loginField;
    }
    
    public InputFieldViewModel getPassField() {
        return passField;
    }
    
    @Override
    protected void onCleared() {
        Log.d("onCleared form view model");
        loginField.onCleared();
        passField.onCleared();
    }
    
}
