package com.ksudev.gitmvvm.screen.form.models;

import android.graphics.Color;

import androidx.annotation.ColorInt;

@SuppressWarnings("WeakerAccess")
public class InputFieldResources {
    @ColorInt
    private int textColor;
    @ColorInt
    private int errorColor;
    @ColorInt
    private int hintColor;
    @ColorInt
    private int focusedColor;
    
    public InputFieldResources() {
        this.textColor = Color.BLACK;
        this.errorColor = Color.BLACK;
        this.hintColor = Color.BLACK;
        this.focusedColor = Color.BLACK;
    }
    
    public InputFieldResources(int textColor, int errorColor, int hintColor, int focusedColor) {
        this.textColor = textColor;
        this.errorColor = errorColor;
        this.hintColor = hintColor;
        this.focusedColor = focusedColor;
    }
    
    @ColorInt
    public int getTextColor() {
        return textColor;
    }
    
    @ColorInt
    public int getErrorColor() {
        return errorColor;
    }
    
    @ColorInt
    public int getHintColor() {
        return hintColor;
    }
    
    @ColorInt
    public int getFocusedColor() {
        return focusedColor;
    }
}
