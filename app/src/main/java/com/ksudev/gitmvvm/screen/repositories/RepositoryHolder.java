package com.ksudev.gitmvvm.screen.repositories;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.ksudev.gitmvvm.R;
import com.ksudev.gitmvvm.content.Repository;

/**
 * @author nevazno
 */
public class RepositoryHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.repositoryName)
    TextView mName;

    @BindView(R.id.repositoryLanguage)
    TextView mLanguage;

    public RepositoryHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(@NonNull Repository repository) {
        mName.setText(repository.getName());
        mLanguage.setText(repository.getLanguage());
    }
}
