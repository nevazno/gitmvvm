package com.ksudev.gitmvvm.screen.auth;

import com.ksudev.gitmvvm.screen.general.LoadingView;

/**
 * @author nevazno
 */
public interface IAuthView extends LoadingView {

    void openRepositoriesScreen();

    void showLoginError();

    void showPasswordError();

}