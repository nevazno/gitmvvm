package com.ksudev.gitmvvm.screen.form.models;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.ksudev.gitmvvm.drawables.UnderlineDrawable;
import com.ksudev.gitmvvm.logger.Log;
import com.ksudev.gitmvvm.utils.validators.IValidator;

@SuppressWarnings({"WeakerAccess", "unused"})
public class InputFieldViewModel implements View.OnFocusChangeListener {
    private IValidator validator;
    
    private ObservableInt textColor = new ObservableInt(Color.BLACK);
    private ObservableInt errorColor = new ObservableInt(Color.BLACK);
    private ObservableInt hintColor = new ObservableInt(Color.BLACK);
    private ObservableField<UnderlineDrawable> background = new ObservableField<>(UnderlineDrawable.newInstance());
    private ObservableField<CharSequence> text = new ObservableField<>();
    private ObservableField<CharSequence> hint = new ObservableField<>();
    private ObservableField<CharSequence> error = new ObservableField<>();
    private ObservableBoolean hasFocus = new ObservableBoolean(false);
    
    public InputFieldViewModel() {
        this.error.set(null);
        ResourcesLiveData resourcesLiveData = new ResourcesLiveData();
        resourcesLiveData.observeForever(res -> {
            
            textColor.set(res.getTextColor());
            errorColor.set(res.getErrorColor());
            hintColor.set(res.getHintColor());
            
            UnderlineDrawable drawable = this.background.get();
            if (drawable != null) {
                drawable.updateColors(res.getHintColor(), res.getFocusedColor());
                Log.d("get resources .edit_drawable+" + drawable);
                this.background.set(drawable);
                this.background.notifyChange();
                Log.d("get resources after notifyChange. edit_drawable+" + drawable);
            }
        });
        Log.d("loadResource");
        resourcesLiveData.loadResource();
    }
    
    public void setValidator(IValidator validator) {
        this.validator = validator;
    }
    
    public ObservableInt getTextColor() {
        return textColor;
    }
    
    public ObservableInt getErrorColor() {
        return errorColor;
    }
    
    public ObservableField<UnderlineDrawable> getBackground() {
        return background;
    }
    
    public ObservableInt getHintColor() {
        return hintColor;
    }
    
    protected void onCleared() {
        Log.d("onCleared field:" + hint.get());
        validator = null;
    }
    
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        this.hasFocus.set(hasFocus);
        CharSequence errorText = null;
        if (!hasFocus) {
            if (!validator.isValid(text.get())) {
                errorText = validator.getErrorMessage("\uD83D\uDE00\uD83D\uDE00");
            }
        }
        error.set(errorText);
        
        UnderlineDrawable drawable = this.background.get();
        if (drawable != null) {
            if (hasFocus) {
                drawable.clearTintColor();
            } else {
                if (!TextUtils.isEmpty(errorText)) {
                    drawable.setTintColor(this.errorColor.get());
                    error.set(validator.getErrorMessage("\uD83D\uDE00\uD83D\uDE00"));
                }
            }
        }
        
    }
    
    public View.OnFocusChangeListener getOnFocusChange() {
        return this;
    }
    
    public ObservableField<CharSequence> getText() {
        return text;
    }
    
    public ObservableField<CharSequence> getHint() {
        return hint;
    }
    
    public void setHint(CharSequence hint) {
        this.hint.set(hint);
    }
    
    public ObservableField<CharSequence> getError() {
        return error;
    }
    
    public ObservableBoolean getHasFocus() {
        return hasFocus;
    }
    
}
