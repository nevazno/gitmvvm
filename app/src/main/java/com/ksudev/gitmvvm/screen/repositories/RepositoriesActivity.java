package com.ksudev.gitmvvm.screen.repositories;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;

import androidx.databinding.library.baseAdapters.BR;
import com.ksudev.gitmvvm.R;
import com.ksudev.gitmvvm.databinding.ActivityAuthBinding;
import com.stfalcon.androidmvvmhelper.mvvm.activities.BindingActivity;

/**
 * @author nevazno
 */
public class RepositoriesActivity extends BindingActivity<ActivityAuthBinding, RepositoryActivityVM> {

    public static void start(@NonNull Activity activity) {
        Intent intent = new Intent(activity, RepositoriesActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public RepositoryActivityVM onCreate() {
        return new RepositoryActivityVM(this);
    }

    @Override
    public int getVariable() {
        return BR.repositoriesViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_repositories;
    }
}
