package com.ksudev.gitmvvm.screen.repositories;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ksudev.gitmvvm.R;
import com.ksudev.gitmvvm.content.Repository;
import com.ksudev.gitmvvm.repository.RepositoryProvider;
import com.ksudev.gitmvvm.screen.commits.CommitsActivity;
import com.ksudev.gitmvvm.widget.BaseAdapter;
import com.stfalcon.androidmvvmhelper.mvvm.activities.ActivityViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class RepositoryActivityVM extends ActivityViewModel<RepositoriesActivity> implements RepositoriesView,
        BaseAdapter.OnItemClickListener<Repository> {
    
    public final RecyclerConfiguration recyclerConfiguration = new RecyclerConfiguration();
    public ObservableBoolean isLoading = new ObservableBoolean();
    public ObservableInt countItems = new ObservableInt();
    private RepositoriesAdapter mAdapter;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    
    RepositoryActivityVM(RepositoriesActivity activity) {
        super(activity);
        activity.setSupportActionBar(activity.findViewById(R.id.toolbar));
        mAdapter = new RepositoriesAdapter(new ArrayList<>());
        mAdapter.setOnItemClickListener(this);
        initRecycler();
        getRepositories();
    }
    
    private void initRecycler() {
        recyclerConfiguration.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerConfiguration.setLayoutManager(new LinearLayoutManager(
                activity, LinearLayoutManager.VERTICAL, false)
        );
        recyclerConfiguration.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                activity, LinearLayoutManager.VERTICAL
        );
        recyclerConfiguration.setDividerItemDecoration(mDividerItemDecoration);
        recyclerConfiguration.setAdapter(mAdapter);
    }
    
    private void getRepositories() {
        Disposable disposable = RepositoryProvider.provideGithubRepository()
                .repositories()
                .doOnSubscribe(disposable1 -> showLoading())
                .doOnTerminate(this::hideLoading)
                .subscribe(this::showRepositories, this::showError);
        compositeDisposable.add(disposable);
    }
    
    @Override
    public void showRepositories(@NonNull List<Repository> repositories) {
        Log.e("showRepositories", "repositories  = " + repositories.size());
        countItems.set(repositories.size());
        mAdapter.changeDataSet(repositories);
    }
    
    @Override
    public void showCommits(@NonNull Repository repository) {
        CommitsActivity.start(activity, repository);
    }
    
    @Override
    public void showError(Throwable throwable) {
        mAdapter.clear();
        Toast.makeText(
                activity,
                throwable.getMessage(),
                Toast.LENGTH_SHORT
        ).show();
    }
    
    @Override
    public void showLoading() {
        isLoading.set(true);
    }
    
    @Override
    public void hideLoading() {
        isLoading.set(false);
    }
    
    @Override
    public void onItemClick(@NonNull Repository repository) {
        showCommits(repository);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
