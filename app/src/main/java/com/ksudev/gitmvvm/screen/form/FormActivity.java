package com.ksudev.gitmvvm.screen.form;

import android.os.Bundle;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.ViewModelProviders;

import com.ksudev.gitmvvm.R;
import com.ksudev.gitmvvm.databinding.FormBinding;
import com.ksudev.gitmvvm.screen.form.factory.FormViewModelFactory;
import com.ksudev.gitmvvm.screen.form.models.FormViewModel;


@SuppressWarnings("FieldCanBeLocal")
public class FormActivity extends AppCompatActivity implements LifecycleObserver {
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FormBinding binding = DataBindingUtil.setContentView(this, R.layout.form_activity);
        binding.setLifecycleOwner(this);
        FormViewModel formViewModel = ViewModelProviders.of(this, FormViewModelFactory.getInstance(this)).get(FormViewModel.class);
        binding.setF(formViewModel);
        binding.loginField.setField(formViewModel.getLoginField());
        binding.passField.setField(formViewModel.getPassField());
        
    }
    
}
