package com.ksudev.gitmvvm.screen.auth;

import android.text.TextUtils;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.ksudev.gitmvvm.AppDelegate;
import com.ksudev.gitmvvm.binding.Handler;
import com.ksudev.gitmvvm.repository.RepositoryProvider;
import com.ksudev.gitmvvm.screen.repositories.RepositoriesActivity;
import com.ksudev.gitmvvm.utils.Constants;
import com.ksudev.gitmvvm.utils.PreferenceUtils;
import com.orhanobut.hawk.Hawk;
import com.stfalcon.androidmvvmhelper.mvvm.activities.ActivityViewModel;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class AuthActivityVM extends ActivityViewModel<AuthActivity> {
    
    public String login;
    public String pass;
    public boolean isRemember = Hawk.get(Constants.REMEMBER, true);
    public ObservableBoolean isLoading = new ObservableBoolean();
    public ObservableField<String> loginError = new ObservableField<>();
    public ObservableField<String> passError = new ObservableField<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    
    public AuthActivityVM(AuthActivity activity) {
        super(activity);
        getActivity().getBinding().setHandler(new Handler());
        String token = PreferenceUtils.getToken();
        if (!TextUtils.isEmpty(token)) {
            openRepositoriesScreen();
        }
        activity.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton checkedRadioButton = group.findViewById(checkedId);
            if (checkedRadioButton.isChecked()) {
                AppDelegate.getTranslateManager().setLng(checkedRadioButton.getText().toString().toLowerCase());
            }
        });
        initLoginAndPass();
        initRadioGroup();
    }
    
    private void initRadioGroup() {
        String lng = AppDelegate.getTranslateManager().getLng();
        switch (lng) {
            case "en":
                activity.radioButtonEn.setChecked(true);
                break;
            case "ru":
                activity.radioButtonEn.setChecked(true);
                break;
            default:
                activity.radioButtonEn.setChecked(true);
        }
    }
    
    public void onLogInButtonClick() {
        tryLogIn(login, pass);
    }
    
    private void tryLogIn(@NonNull String login, @NonNull String password) {
        if (TextUtils.isEmpty(login)) {
            loginError.set("Login is empty, send from AuthActivityVM");
        }
        if (TextUtils.isEmpty(password)) {
            passError.set("Password is empty, send from AuthActivityVM");
        }
        if (TextUtils.isEmpty(loginError.get()) && TextUtils.isEmpty(passError.get())) {
            Hawk.put(Constants.REMEMBER, isRemember);
            if (isRemember) {
                saveLoginAndPass(login, password);
            } else {
                clearLoginAndPass();
            }
            Disposable disposable = RepositoryProvider.provideGithubRepository()
                    .auth(login, password)
                    .doOnSubscribe(disposable1 -> AuthActivityVM.this.setProgress(true))
                    .doOnTerminate(() -> setProgress(false))
                    .subscribe(authorization -> openRepositoriesScreen(), this::showLoginError);
            compositeDisposable.add(disposable);
        }
    }
    
    private void initLoginAndPass() {
        login = Hawk.get(Constants.LOGIN);
        pass = Hawk.get(Constants.PASS);
    }
    
    private void saveLoginAndPass(String login, String password) {
        Hawk.put(Constants.LOGIN, login);
        Hawk.put(Constants.PASS, password);
    }
    
    private void clearLoginAndPass() {
        Hawk.remove(Constants.LOGIN);
        Hawk.remove(Constants.PASS);
    }
    
    private void setProgress(boolean flag) {
        isLoading.set(flag);
    }
    
    private void openRepositoriesScreen() {
        RepositoriesActivity.start(activity);
        activity.finish();
    }
    
    private void showLoginError(Throwable throwable) {
        Toast.makeText(
                activity,
                throwable.getMessage(),
                Toast.LENGTH_SHORT
        ).show();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
