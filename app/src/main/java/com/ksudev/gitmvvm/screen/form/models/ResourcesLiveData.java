package com.ksudev.gitmvvm.screen.form.models;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;

import com.ksudev.gitmvvm.AppDelegate;
import com.ksudev.gitmvvm.R;

@SuppressWarnings("WeakerAccess")
public class ResourcesLiveData extends MutableLiveData<InputFieldResources> {
    
    public void loadResource() {
        Context context = AppDelegate.getContext();
        int hintColor = ContextCompat.getColor(context, R.color.form_edit_text_default);
        int focusedColor = ContextCompat.getColor(context, R.color.form_edit_text_focused);
        int errorColor = ContextCompat.getColor(context, R.color.form_edit_text_error);
        int textColor = ContextCompat.getColor(context, R.color.form_edit_text);
        setValue(new InputFieldResources(textColor, errorColor, hintColor, focusedColor));
    }
    
    
}
