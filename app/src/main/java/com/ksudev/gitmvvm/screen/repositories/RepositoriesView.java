package com.ksudev.gitmvvm.screen.repositories;

import androidx.annotation.NonNull;

import java.util.List;

import com.ksudev.gitmvvm.content.Repository;
import com.ksudev.gitmvvm.screen.general.LoadingView;

/**
 * @author nevazno
 */
public interface RepositoriesView extends LoadingView {

    void showRepositories(@NonNull List<Repository> repositories);

    void showCommits(@NonNull Repository repository);

    void showError(Throwable throwable);
}
