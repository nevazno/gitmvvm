package com.ksudev.gitmvvm.screen.commits;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ksudev.gitmvvm.screen.repositories.RecyclerConfiguration;
import com.ksudev.gitmvvm.screen.repositories.RepositoriesAdapter;
import com.stfalcon.androidmvvmhelper.mvvm.activities.ActivityViewModel;

public class CommitsActivityVM extends ActivityViewModel<CommitsActivity> {

    public final RecyclerConfiguration recyclerConfiguration = new RecyclerConfiguration();
    private RepositoriesAdapter mAdapter;
    public ObservableBoolean isLoading = new ObservableBoolean();
    public ObservableInt countItems = new ObservableInt();

    public CommitsActivityVM(CommitsActivity activity, String repositoryName) {
        super(activity);
        activity.setSupportActionBar(activity.mToolbar);
        initRecycler();
        Snackbar.make(activity.mRecyclerView, "Not implemented for " + repositoryName + " yet", Snackbar.LENGTH_LONG).show();
    }

    private void initRecycler() {
        recyclerConfiguration.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerConfiguration.setLayoutManager(new LinearLayoutManager(
                activity, LinearLayoutManager.VERTICAL, false)
        );
        recyclerConfiguration.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                activity, LinearLayoutManager.VERTICAL
        );
        recyclerConfiguration.setDividerItemDecoration(mDividerItemDecoration);
        recyclerConfiguration.setAdapter(mAdapter);
    }
}
