package com.ksudev.gitmvvm.screen.form.factory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.ksudev.gitmvvm.logger.Log;

public class InputFieldViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    
    private static InputFieldViewModelFactory instance = null;
    
    @NonNull
    public static synchronized InputFieldViewModelFactory getInstance() {
        if (instance == null) {
            instance = new InputFieldViewModelFactory();
        }
        return instance;
    }
    
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        try {
            return modelClass.newInstance();
        } catch (IllegalAccessException e) {
            Log.e(e);
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            Log.e(e);
            throw new RuntimeException(e);
        }
    }
}
