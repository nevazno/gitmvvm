package com.ksudev.gitmvvm.screen.walkthrough;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ksudev.gitmvvm.content.Benefit;

import java.util.List;

/**
 * @author nevazno
 */
public class WalkthroughAdapter extends FragmentStatePagerAdapter {
    
    private final List<Benefit> mBenefits;
    
    public WalkthroughAdapter(FragmentManager fm, @NonNull List<Benefit> benefits) {
        super(fm);
        mBenefits = benefits;
    }
    
    @Override
    public Fragment getItem(int position) {
        return WalkthroughBenefitFragment.create(mBenefits.get(position));
    }
    
    @Override
    public int getCount() {
        return mBenefits.size();
    }
}
