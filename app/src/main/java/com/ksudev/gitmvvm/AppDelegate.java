package com.ksudev.gitmvvm;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.ksudev.gitmvvm.api.ApiFactory;
import com.ksudev.gitmvvm.manager.TranslateManager;
import com.ksudev.gitmvvm.repository.RepositoryProvider;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;
import com.orhanobut.hawk.LogLevel;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class AppDelegate extends Application {
    
    private static AppDelegate instance;
    private TranslateManager translateManager;
    
    @NonNull
    public static Context getContext() {
        return instance;
    }
    
    public static TranslateManager getTranslateManager() {
        return instance.translateManager;
    }
    
    public static File getAppCacheDir() {
        return instance.getCacheDir();
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        
        Hawk.init(this)
                .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
                .setStorage(HawkBuilder.newSharedPrefStorage(this))
                .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
                .build();
        
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
//                .rxFactory(new RealmObservableFactory())
                .build();
        Realm.setDefaultConfiguration(configuration);
        
        ApiFactory.recreate();
        RepositoryProvider.init();
        
        translateManager = new TranslateManager();
        translateManager.init();
    }
    
    @Override
    public void onTerminate() {
        super.onTerminate();
        translateManager.onDestroy();
    }
}
