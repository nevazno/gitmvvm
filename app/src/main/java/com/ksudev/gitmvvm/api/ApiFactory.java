package com.ksudev.gitmvvm.api;

import androidx.annotation.NonNull;

import com.ksudev.gitmvvm.AppDelegate;
import com.ksudev.gitmvvm.BuildConfig;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author nevazno
 */
public final class ApiFactory {

    private static OkHttpClient sClient;
    private static OkHttpClient loggingClient;
    private static final long CACHE_SIZE = 16777216; //16 * 1024 * 1024

    private static volatile GithubService gitHubService;
    private static volatile MasterService masterService;

    private ApiFactory() {
    }

    @NonNull
    public static GithubService getGithubService() {
        GithubService service = gitHubService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = gitHubService;
                if (service == null) {
                    service = gitHubService = buildGitHubRetrofit().create(GithubService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    public static MasterService getMasterService() {
        MasterService service = masterService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = masterService;
                if (service == null) {
                    service = masterService = buildMasterRetrofit().create(MasterService.class);
                }
            }
        }
        return service;
    }

    public static void recreate() {
        sClient = null;
        sClient = getClient();
        loggingClient = null;
        loggingClient = getLoggingClient();
        gitHubService = buildGitHubRetrofit().create(GithubService.class);
        masterService = buildMasterRetrofit().create(MasterService.class);
    }

    @NonNull
    private static Retrofit buildMasterRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.MASTER_API)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @NonNull
    private static Retrofit buildGitHubRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.GITHUB_API)
                .client(BuildConfig.DEBUG ? getLoggingClient() : getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }

    @NonNull
    private static OkHttpClient getLoggingClient() {
        OkHttpClient client = loggingClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = loggingClient;
                if (client == null) {
                    client = loggingClient = buildLoggingClient();
                }
            }
        }
        return client;
    }

    @NonNull
    private static OkHttpClient buildLoggingClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(LoggingInterceptor.create())
                .addInterceptor(ApiKeyInterceptor.create())
                .build();
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
//                .addInterceptor(LoggingInterceptor.create())
                .cache(new Cache(AppDelegate.getAppCacheDir(), CACHE_SIZE))
                .build();
    }
}
