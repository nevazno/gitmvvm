package com.ksudev.gitmvvm.api;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;

public interface MasterService {
    
    @GET("api/v1/translates")
    Observable<Response<Object>> loadTranslate();
}
