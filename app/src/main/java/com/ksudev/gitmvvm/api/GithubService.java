package com.ksudev.gitmvvm.api;

import com.google.gson.JsonObject;
import com.ksudev.gitmvvm.content.Authorization;
import com.ksudev.gitmvvm.content.CommitResponse;
import com.ksudev.gitmvvm.content.Repository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * @author nevazno
 */
public interface GithubService {
    
    @POST("/authorizations")
    Observable<Authorization> authorize(@Header("Authorization") String authorization,
                                        @Body JsonObject params);
    
    @GET("/user/repos")
    Observable<List<Repository>> repositories();
    
    @GET("/repos/{user}/{repo}/commits")
    Observable<List<CommitResponse>> commits(@Path("user") String user, @Path("repo") String repo);
    
}
