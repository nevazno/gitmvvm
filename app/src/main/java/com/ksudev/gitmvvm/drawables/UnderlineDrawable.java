package com.ksudev.gitmvvm.drawables;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.StateSet;
import android.view.Gravity;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.graphics.drawable.DrawableWrapper;
import androidx.core.content.ContextCompat;

import com.ksudev.gitmvvm.AppDelegate;
import com.ksudev.gitmvvm.R;
import com.ksudev.gitmvvm.logger.Log;
import com.ksudev.gitmvvm.utils.animation.IntProperty;


/**
 * Created by andrew on 21.11.14.
 */
@SuppressWarnings({"unused", "FieldCanBeLocal", "WeakerAccess"})
public class UnderlineDrawable extends DrawableWrapper implements Animatable {
    public static final int BACKGROUND_INDEX = 0;
    public static final int LINE_INDEX = 1;
    public static final int LINE_ACTIVATED_INDEX = 2;
    public static final int MAX_LEVEL = 10000;
    public static final int MIDDLE_LEVEL = 5000;
    public static final int MIN_LEVEL = 0;
    private static final TimeInterpolator LINEAR_INTERPOLATOR = new AccelerateDecelerateInterpolator();
    private static final int DURATION = 700;
    private final ClipProperty CLIP_PROPERTY = new ClipProperty("clip_property") {
        @Override
        public void setValue(UnderlineDrawable object, int value) {
            object.setLevel(value);
        }
        
        @Override
        public Integer get(UnderlineDrawable object) {
            return object.getLevel();
        }
    };
    private final Rect mTmpRect = new Rect();
    private Context context = AppDelegate.getContext();
    private ObjectAnimator mAnimator;
    private Drawable background;
    private Drawable line;
    private Drawable lineActivated;
    private int mHotSpotX;
    private boolean mFocused = false;
    private boolean mPressed = false;
    
    public UnderlineDrawable(LayerDrawable layerDrawable) {
        super(layerDrawable);
        background = layerDrawable.getDrawable(BACKGROUND_INDEX);
        line = layerDrawable.getDrawable(LINE_INDEX);
        lineActivated = layerDrawable.getDrawable(LINE_ACTIVATED_INDEX);
        int horizontal = context.getResources().getDimensionPixelSize(R.dimen.edit_text_inset_horizontal_material);
        int top = context.getResources().getDimensionPixelSize(R.dimen.edit_text_inset_top_material);
        int bottom = context.getResources().getDimensionPixelSize(R.dimen.edit_text_inset_bottom_material);
        getWrappedDrawable().setLayerInset(BACKGROUND_INDEX, horizontal, top, horizontal, bottom);
        getWrappedDrawable().setLayerInset(LINE_INDEX, horizontal, top, horizontal, bottom);
        getWrappedDrawable().setLayerInset(LINE_ACTIVATED_INDEX, horizontal, top, horizontal, bottom);
    }
    
    public static UnderlineDrawable newInstance() {
        Context context = AppDelegate.getContext();
        Resources res = context.getResources();
        LayerDrawable drawableDelegate = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.edit_drawable);
        return (UnderlineDrawable) new UnderlineDrawable(drawableDelegate).mutate();
    }
    
    @Override
    public LayerDrawable getWrappedDrawable() {
        return (LayerDrawable) super.getWrappedDrawable();
    }
    
    public void jumpToFinal() {
        if (mAnimator != null) {
            mAnimator.end();
            mAnimator = null;
        }
    }
    
    @Override
    public void setHotspot(float x, float y) {
        mHotSpotX = (int) x;
    }
    
    @Override
    public void draw(@NonNull Canvas canvas) {
        background.draw(canvas);
        line.draw(canvas);
        final Rect r = mTmpRect;
        final Rect bounds = background.getBounds();
        int count = canvas.save();
        canvas.clipRect(bounds);
        final int level = getLevel();
        if (mFocused && level > 0) {
            {
                int translate = mHotSpotX > 0 ? (bounds.right - mHotSpotX) : (int) (bounds.width() * 0.5f);
                float value = 1f - (level * 1f / MAX_LEVEL);
                int w = bounds.width();
                w -= (int) (w * Math.abs(value));
                
                int h = bounds.height();
                int gravity = value < 0 ? Gravity.START : Gravity.END;
                Gravity.apply(gravity, w, h, bounds, 0, 0, r);
                
                if (w > 0 && h > 0) {
                    int save = canvas.save();
                    canvas.translate(-translate, 0);
                    canvas.clipRect(r);
                    lineActivated.draw(canvas);
                    canvas.restoreToCount(save);
                }
            }
            
            {
                int translate = mHotSpotX > 0 ? (mHotSpotX - bounds.left) : (int) (bounds.width() * 0.5f);
                float value = (level * 1f / MAX_LEVEL) - 1f;
                int w = bounds.width();
                w -= (int) (w * Math.abs(value));
                int h = bounds.height();
                
                int gravity = value < 0 ? Gravity.START : Gravity.END;
                Gravity.apply(gravity, w, h, bounds, r);
                
                if (w > 0 && h > 0) {
                    int save = canvas.save();
                    canvas.translate(translate, 0);
                    canvas.clipRect(r);
                    lineActivated.draw(canvas);
                    canvas.restoreToCount(save);
                }
            }
        }
        canvas.restoreToCount(count);
    }
    
    @Override
    protected boolean onLevelChange(int level) {
        super.onLevelChange(level);
        invalidateSelf();
        return true;
    }
    
    private String rectToString(Rect r) {
        return "{left=" + r.left + " width=" + r.width() + "}";
    }
    
    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        super.setColorFilter(colorFilter);
    }
    
    @Override
    public boolean setState(int[] stateSet) {
        return onStateChange(stateSet);
    }
    
    @Override
    protected boolean onStateChange(int[] stateChange) {
        boolean changed = false;
        Log.d(" onStateChange -> " + StateSet.dump(stateChange));
        boolean pressed = false;
        boolean focused = false;
        boolean hovered = false;
        for (int state : stateChange) {
            if (state == android.R.attr.state_pressed) {
                pressed = true;
            }
            if (state == android.R.attr.state_focused) {
                focused = true;
            }
            if (state == android.R.attr.state_hovered) {
                hovered = true;
            }
        }
        if (pressed || focused || hovered) {
            changed = true;
        }
        if (!pressed) {
            if (mFocused == focused) {
                return changed;
            } else {
                onFocusChanged(focused);
            }
        }
        return changed;
    }
    
    private void onFocusChanged(boolean focused) {
        mFocused = focused;
        if (mFocused) {
            start();
        } else {
            stop();
        }
    }
    
    @Override
    public boolean isStateful() {
        return true;
    }
    
    public boolean hasFocusStateSpecified() {
        return true;
    }
    
    @Override
    public void start() {
        changeLevelOnFocused(false);
        if (mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }
        mAnimator = ObjectAnimator.ofInt(this, CLIP_PROPERTY, getLevel(), MAX_LEVEL);
        mAnimator.setDuration(DURATION);
        mAnimator.setInterpolator(LINEAR_INTERPOLATOR);
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mHotSpotX = 0;
                changeLevelOnFocused(true);
                
            }
        });
        mAnimator.start();
    }
    
    private void changeLevelOnFocused(boolean reset) {
        if (mFocused) {
            setLevel(reset ? MAX_LEVEL : getLevel());
        } else {
            setLevel(MIN_LEVEL);
        }
    }
    
    @Override
    public void stop() {
        jumpToFinal();
    }
    
    @Override
    public boolean isRunning() {
        return mAnimator != null && mAnimator.isRunning();
    }
    
    @Override
    public int getChangingConfigurations() {
        return 0;
    }
    
    @Nullable
    @Override
    public ConstantState getConstantState() {
        return null;
    }
    
    public void updateColors(int hintColor, int focusedColor) {
    
    }
    
    public void setTintColor(@ColorInt int colorInt) {
        line.setTint(colorInt);
        lineActivated.setTint(colorInt);
    }
    
    public void clearTintColor() {
        line.setColorFilter(null);
        lineActivated.setColorFilter(null);
    }
    
    private static abstract class ClipProperty extends IntProperty<UnderlineDrawable> {
        ClipProperty(String name) {
            super(name);
        }
    }
}
