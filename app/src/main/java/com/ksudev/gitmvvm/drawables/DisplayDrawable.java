package com.ksudev.gitmvvm.drawables;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.Gravity;

import androidx.annotation.NonNull;

import com.ksudev.gitmvvm.binding.DisplayDrawableComponent;

@SuppressWarnings("WeakerAccess")
public class DisplayDrawable extends Drawable {
    public static final int MAX_LEVEL = 10000;
    public static final int MIDDLE_LEVEL = 5000;
    public static final int MIN_LEVEL = 0;
    private final Rect mTmpRect = new Rect();
    Paint p = newP();
    private DisplayDrawableComponent.OnLevelChangedListener mLevelChangedListener;
    
    public DisplayDrawable() {
    
    }
    
    public static DisplayDrawable newInstance() {
        return new DisplayDrawable();
    }
    
    
    @Override
    protected boolean onLevelChange(int level) {
        super.onLevelChange(level);
        if (mLevelChangedListener != null) {
            mLevelChangedListener.onBackgroundLevelChanged(level);
        }
        invalidateSelf();
        return true;
    }
    
    private Paint newP() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.CYAN);
        paint.setDither(true);
        paint.setAlpha(100);
        return paint;
    }
    
    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawColor(Color.WHITE);
        
        final int level = getLevel();
        if (level > 0) {
            final Rect r = mTmpRect;
            final Rect bounds = getBounds();
            int mHotSpotX = (int) (bounds.left + (bounds.width() * 0.7f));
            
            
            { // Draw the unselected portion
                int translate = mHotSpotX > 0 ? (bounds.right - mHotSpotX) : (int) (bounds.width() * 0.5f);
//                float value = (level * 1f / MIDDLE_LEVEL) - 1f;
                float value = 1f - (level * 1f / MAX_LEVEL);
                int w = bounds.width();
                w -= (int) (w * Math.abs(value));
                
                int h = bounds.height();
                int gravity = value < 0 ? Gravity.START : Gravity.END;
                Gravity.apply(gravity, w, h, bounds, 0, 0, r);
                
                if (w > 0 && h > 0) {
                    canvas.save();
                    canvas.translate(-translate, 0);
                    p.setColor(Color.GREEN);
                    p.setAlpha(100);
                    canvas.drawRect(r, p);
//                    canvas.clipRect(r);
//                    lineActivated.draw(canvas);
                    canvas.restore();
                }
            }
            
            { // Draw the selected portion
                int translate = mHotSpotX > 0 ? (mHotSpotX - bounds.left) : (int) (bounds.width() * 0.5f);
//                float value = 1f - (level * 1f / MAX_LEVEL);
                float value = (level * 1f / MAX_LEVEL) - 1f;
                int w = bounds.width();
                w -= (int) (w * Math.abs(value));
                int h = bounds.height();
                
                int gravity = value < 0 ? Gravity.START : Gravity.END;
                Gravity.apply(gravity, w, h, bounds, r);
                
                if (w > 0 && h > 0) {
                    canvas.save();
                    canvas.translate(translate, 0);
                    p.setColor(Color.RED);
                    p.setAlpha(100);
                    canvas.drawRect(r, p);
//                    canvas.clipRect(r);
//                    lineActivated.draw(canvas);
                    canvas.restore();
                }
            }
        }
        
    }
    
    @Override
    public void setAlpha(int alpha) {
    
    }
    
    
    @Override
    public boolean isStateful() {
        return false;
    }
    
    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        
    }
    
    @Override
    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }
    
    public void setBackgroundLevelChangedListener(DisplayDrawableComponent.OnLevelChangedListener onLevelChangedListener) {
        mLevelChangedListener = onLevelChangedListener;
    }
}
